import serveur.Serveur;


public class TestServeur {
	
	public static void main(String[] args) {
		
		if (args.length != 1)
		{
			System.out.println("erreur arguments. usage : java TestServeyr port");
			System.exit(0);
		}
		
		int port = 0;
		
		try {
			port = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.out.println("Le port doit ?tre un nombre.");
		}

		if (port > 0) {

			Serveur serv = new Serveur(port);
			serv.accepte(); 
			
			serv.envoieClients("Vous �tes connect�s");
			
			while (true)
			{
				serv.envoieClients(serv.action());
				serv.changerJoueur();
			}
			
		}
		
	}
	
}
