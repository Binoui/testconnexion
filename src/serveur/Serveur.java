package serveur;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


/**
 * La classe Server permet ? des clients de se connecter ? lui, puis accepte le
 * client et cr?er un Thread pour chaque client.
 * 
 * Le serveur peut acceuillir un serveur de jeu ainsi qu'un maximum de quatre
 * clients.
 * 
 * @author Julien F.
 * @version 1.0, 04/12/2014
 */
public class Serveur {

	private ServerSocket ss;
	private IJeu jeu;
	
	private int nbClient;
	private int nbMaxClient;
	private int timer;
	private int timeOut;
	private boolean tourVariable;
	private boolean etatInitial;

	private ArrayList<Client> clients;
	private ArrayList<String> ids;
	
	private Client clientCourant;
	private String ordreTour;

	
	public Serveur(int port) {
		nbClient = 0;
		nbMaxClient = 2;
		clients = new ArrayList<Client>();

//		ConfigFile cfg = new ConfigFile();
//		nbMaxClient = cfg.getNumberPlayer();
//		timer = cfg.getTimer();
//		tourVariable = cfg.isVariableTurn();
//		etatInitial = cfg.isInitialState();

		//ids = IDFactory.genererIDs(nbMaxClient);
		ids = new ArrayList<String>();
		ids.add("123|1");
		ids.add("456|2");
		
		
		try {
			ss = new ServerSocket(port);
			ss.setSoTimeout(60000);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setJeu(IJeu jeu) {
		this.jeu = jeu;
	}

	/**
	 * Accepte la connexion des clients tant que l'on a pas atteint le nombre de
	 * joueur maximum.
	 */
	public void accepte() throws NullPointerException {
		
		//if(jeu==null) throw new NullPointerException();
		
		while (nbClient < nbMaxClient) {
			try {
				Socket socket = ss.accept();
				ajouterClient(socket);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
				
		if(etatInitial)
			envoieClients(jeu.getEtatInitial());
		
		
	}

	/**
	 * Ajoute un client
	 * 
	 * @return true si le client est ajout?
	 */
	private boolean ajouterClient(Socket s) {
		if (nbClient == nbMaxClient)
			return false;

		Client c = new Client(s);
		//Format = "idJoueur|numJoueur|nomJoueur"
		String str = c.recevoir();
		String key = str.split("|")[0]+":"+str.split("|")[1];
		c.setNom(str.split("|")[2]);
		if (!ids.contains(key)) {
			ids.remove(key);
			clients.add(c);
		} else {
			c.envoyer("Connexion refus?e");
			c.fermer();
			return false;
		}
		
		if(nbClient==0)
			clientCourant = c;
		
		nbClient++;

		return true;
	}
	
	/**
	 * Envoie le message pass? en param?tre ? tous les client connect?s.
	 * 
	 * @param message
	 *            Message ? envoyer aux clients
	 */
	public void envoieClients(String message) {
		for (Client c : clients) {
			c.envoyer(message);
		}
	}
	
	/**
	 * Demande l'action du client courant et la joue
	 */
	public String action() {
		return clientCourant.recevoir();
	}
	
	/**
	 * Change le client courant
	 */
	public void changerJoueur() {
		clientCourant = clientCourant == clients.get(0) ? clients.get(1) : clients.get(0);
	}
	
	/**
	 * 
	 * @return liste des IDs
	 */
	public ArrayList<String> getIDs() {
		return ids;
	}
	
	public boolean isEtatInitial() {
		return etatInitial;
	}

	public void setEtatInitial(boolean etatInitial) {
		this.etatInitial = etatInitial;
	}

	public int getNbClient() {
		return nbClient;
	}

	public int getNbMaxClient() {
		return nbMaxClient;
	}
	
	public String getJoueurCourant() {
		return clientCourant.getNom();
	}

	public int getTimer() {
		return timer;
	}

	public int getTimeOut() {
		return timeOut;
	}

	public boolean isTourVariable() {
		return tourVariable;
	}

	public String getOrdreTour() {
		return ordreTour;
	}
}
