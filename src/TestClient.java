
public class TestClient {
	public static void main(String[] args) {
		
		if (args.length != 1)
		{
			System.out.println("erreur arguments. usage : java TestClient id");
			System.exit(0);
		}
		
		Connexion co = new Connexion("localhost", 9000);
		String id = args[0];
		co.send(id);		
		
		while (true)
		{
			co.send("salut");
			System.out.println(co.receive());
		}
	}
}
